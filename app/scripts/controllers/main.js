'use strict';

/**
 * @ngdoc function
 * @name myAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myAppApp
 */
angular.module('myAppApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.champions = [];
    $http.get('bower_components/dragontail/6.16.2/data/en_US/champion.json')
    .success(function (data) { 
      for (let key in data.data) {
        $scope.champions.push({id: key, info: data.data[key]});
      }
    });
    
    $scope.modalShown = false;
    $scope.toggleModal = function(ch) {
      $scope.ch = ch;
      slidesCarousel.startSlides($scope.ch);
      $scope.modalShown = !$scope.modalShown;
    };

  // Slides
  var slides, currIndex = 0;

  var slidesCarousel = (function() {
    function startSlides(ch) {
      $scope.myInterval = 0;
      $scope.noWrapSlides = false;
      $scope.active = 0;
      slides = $scope.slides = [];
      currIndex = 0;

      var i = 0;
      while(imageExists('bower_components/dragontail/img/champion/splash/' + ch.id + '_' + i + '.jpg')) {
        $scope.addSlide('bower_components/dragontail/img/champion/splash/' + ch.id + '_' + i + '.jpg');
        i++;
      }
    }

    function assignNewIndexesToSlides(indexes) {
      for (var i = 0, l = slides.length; i < l; i++) {
        slides[i].id = indexes.pop();
      }
    }

    function generateIndexesArray() {
      var indexes = [];
      for (var i = 0; i < currIndex; ++i) {
        indexes[i] = i;
      }
      return shuffle(indexes);
    }

    // http://stackoverflow.com/questions/962802#962890
    function shuffle(array) {
      var tmp, current, top = array.length;

      if (top) {
        while (--top) {
          current = Math.floor(Math.random() * (top + 1));
          tmp = array[current];
          array[current] = array[top];
          array[top] = tmp;
        }
      }

      return array;
    }

    // Checking 404 champion image
    function imageExists(imageUrl){
      var http = new XMLHttpRequest();
      http.open('HEAD', imageUrl, false);
      http.send();
      return http.status !== 404;
    }

    return {
      startSlides: startSlides,
      assignNewIndexesToSlides: assignNewIndexesToSlides,
      generateIndexesArray: generateIndexesArray
    }
  })();
  
  $scope.addSlide = function(img) {
    var newWidth = 600 + slides.length + 1;
    slides.push({
      image: img || '//unsplash.it/' + newWidth + '/300',
      text: ['Nice image','Awesome photograph','That is so cool','I love that'][slides.length % 4],
      id: currIndex++
    });
    console.log(currIndex);
  };

  $scope.randomize = function() {
    var indexes = slidesCarousel.generateIndexesArray();
    slidesCarousel.assignNewIndexesToSlides(indexes);
  };
});
